<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\LengthAwarePaginator;

class ProductService
{
    public function productsByCategory(string $categoryId, array $filters = []): LengthAwarePaginator
    {
        return Product::whereHas(
            'categories',
            fn (Builder $builder) => $builder->whereCategoryId($categoryId)
        )
            ->paginate($filters['per_page'] ?? 15);
    }
}
