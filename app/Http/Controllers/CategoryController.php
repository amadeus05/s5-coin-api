<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Resources\Categories\CategoryResourceCollection;

class CategoryController extends Controller
{
    public function index()
    {
        return new CategoryResourceCollection(Category::paginate(request('per_page', 15)));
    }
}
