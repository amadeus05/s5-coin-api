<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Response;
use App\Http\Requests\Products\ProductIndexRequest;
use App\Http\Requests\Products\ProductsByCategoryRequest;
use App\Http\Resources\Products\ProductResourceCollection;
use App\Services\ProductService;

class ProductController extends Controller
{
    public function getProductsByCategory(string $categoryId, ProductsByCategoryRequest $request, ProductService $productService): Response
    {
        return new Response(new ProductResourceCollection($productService->productsByCategory($categoryId, $request->validated())));
    }

    public function index(ProductIndexRequest $request): Response
    {
        return new Response(new ProductResourceCollection(Product::paginate($request->per_page ?? 15)));
    }
}
