<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('categories', [CategoryController::class, 'index']); //http://localhost/api/categories?per_page=100

Route::prefix('products')->group(function () {
    Route::get('', [ProductController::class, 'index']); // http://localhost/api/products/categories/2?per_page=100
    Route::get('categories/{category_id}', [ProductController::class, 'getProductsByCategory']); // http://localhost/api/products/categories/2?per_page=100
});
