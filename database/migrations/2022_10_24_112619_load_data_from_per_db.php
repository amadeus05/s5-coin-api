<?php


use App\Models\Product;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::transaction(function () {
            $this->loadCategoriesFromCsv();
            $this->loadProductsFromCsv();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
    }

    private function loadCategoriesFromCsv()
    {
        //csv file records starting from id 2 but the record with id 2 link to parent with id 1 
        //so we must use nullable for parent id or create parent category with id 1 so that our migration works properly i will do both =)

        $defaultCategory = new Category();
        $defaultCategory->name = 'Default';
        $defaultCategory->name = 'default';
        $defaultCategory->save();

        $categories = [];

        if (($open = fopen(storage_path() . "/category.csv", "r")) !== false) {

            while (($data = fgetcsv($open, 1000, ",")) !== false) {
                $categories[] = $data;
            }

            fclose($open);
        }

        $columnNames = true;

        collect($categories)->each(function ($item) use (&$columnNames) {

            if ($columnNames) {
                $columnNames = false;
                return;
            }

            $category              = new Category();
            $category->id          = $item[0];
            $category->name        = $item[1];
            $category->url_key     = $item[2];
            $category->description = $item[3];
            $category->image       = $item[4];
            $category->parent_id   = $item[5];

            $parentCategory = Category::find($item[5]);

            if (!$parentCategory) {
                $category->parent_id = null;
                //also lets log broken categories, then based on this logs wi wil decide what to do with them
                Log::debug('Categories parsing: not existed category ID: ' . $item[5]);
            }

            $category->save();
        });
    }

    private function loadProductsFromCsv()
    {
        $products = [];

        if (($open = fopen(storage_path() . "/product.csv", "r")) !== false) {

            while (($data = fgetcsv($open, 3000, ",")) !== false) {
                $products[] = $data;
            }

            fclose($open);
        }

        $skipColumnNames = true;

        foreach ($products as $key => $value) {

            if ($skipColumnNames) {
                $skipColumnNames = false;
                continue;
            }

            $product                                 = new Product();
            $product->id                             = $value[0];
            $product->type_id                        = $value[1];
            $product->sku                            = $value[2];
            $product->opera_sku                      = $value[3] == '' ? null : $value[3];
            $product->old_sku                        = $value[4] == '' ? null : $value[4];
            $product->override_opera                 = $value[5] == '' ? null : $value[5];
            $product->name                           = $value[6] == '' ? null : $value[6];
            $product->inlet                          = is_numeric($value[7]) ? $value[7] : null;
            $product->outlet                         = is_numeric($value[8]) ? $value[8] : null;
            $product->hose_type                      = is_numeric($value[9]) ? $value[9] : null;
            $product->angle_in_deg                   = is_numeric($value[10]) ? $value[10] : null;
            $product->max_lpm                        = is_numeric($value[11]) ? $value[11] : null;
            $product->voltage                        = is_numeric($value[12]) ? $value[12] : null;
            $product->material                       = is_numeric($value[13]) ? $value[13] : null;
            $product->bar                            = is_numeric($value[14]) ? $value[14] : null;
            $product->o_ring_thickness               = is_numeric($value[15]) ? $value[15] : null;
            $product->diameter                       = is_numeric($value[16]) ? $value[16] : null;
            $product->colour                         = is_numeric($value[17]) ? $value[17] : null;
            $product->rpm                            = is_numeric($value[17]) ? $value[17] : null;
            $product->status                         = $value[19];
            $product->url_key                        = $value[20];
            $product->visibility                     = $value[21];
            $product->clearance                      = is_numeric($value[22]) ? $value[22] : null;
            $product->max_temperature                = is_numeric($value[23]) ? $value[23] : null;
            $product->description                    = $value[24];
            $product->short_description              = $value[25];
            $product->tech_spec_1                    = ($value[26] == '') ? null : $value[26];
            $product->tech_spec_2                    = ($value[27] == '') ? null : $value[27];
            $product->tech_spec_3                    = ($value[28] == '') ? null : $value[28];
            $product->product_videos                 = $value[29];
            $product->nozzle_value                   = is_numeric($value[30]) ? $value[30] : null;
            $product->nozzle_size                    = is_numeric($value[31]) ? $value[31] : null;
            $product->foam_value                     = ($value[32] == '') ? null : $value[32];
            $product->is_featured                    = is_numeric($value[33]) ? $value[33] : null;
            $product->featured_position              = ($value[34] == '') ? null : $value[34];
            $product->hose_clamp_size                = $value[35];
            $product->office_size                    = $value[36];
            $product->shoe_size                      = $value[37];
            $product->thread                         = $value[38];
            $product->size_and_angle                 = $value[39];
            $product->inlet_outlet                   = $value[40];
            $product->clothing_size                  = $value[41];
            $product->wheel_style                    = $value[42];
            $product->flow_and_pressure              = $value[43];
            $product->country_of_manufacture         = $value[44];
            $product->select_nozzle_size             = is_numeric($value[45]) ? $value[45] : null;
            $product->dn_internal_diameter           = is_numeric($value[46]) ? $value[46] : null;
            $product->currency                       = $value[47];
            $product->pack_size                      = $value[48];
            $product->easyturn                       = $value[49];
            $product->priority                       = $value[50];
            $product->manufacturer_number_1          = $value[51];
            $product->manufacturer_number_2          = $value[52];
            $product->manufacturer_number_3          = $value[53];
            $product->manufacturer_number_4          = $value[54];
            $product->manufacturer_number_5          = $value[55];
            $product->manufacturer_number_6          = $value[56];
            $product->manufacturer_number_7          = $value[57];
            $product->manufacturer_number_8          = $value[58];
            $product->manufacturer_number_9          = $value[59];
            $product->manufacturer_number_10         = $value[60];
            $product->hose_application               = is_numeric($value[61]) ?? null;
            $product->hose_inlet                     = is_numeric($value[62]) ?? null;
            $product->hose_outlet                    = is_numeric($value[63]) ?? null;
            $product->hose_length                    = is_numeric($value[64]) ?? null;
            $product->hose_colour                    = is_numeric($value[65]) ?? null;
            $product->price                          = $value[66];
            $product->special_price                  = $value[67];
            $product->poa                            = $value[68];
            $product->poa_price                      = $value[69];
            $product->msrp                           = $value[70];
            $product->meta_title                     = $value[71];
            $product->meta_keywords                  = $value[72];
            $product->meta_description               = $value[73];
            $product->pdf_title_1                    = $value[74];
            $product->pdf_title_2                    = $value[75];
            $product->pdf_title_3                    = $value[76];
            $product->pdf_title_4                    = $value[77];
            $product->categories                     = $value[78];
            $product->bullet_point_1                 = $value[79];
            $product->bullet_point_2                 = $value[80];
            $product->bullet_point_3                 = $value[81];
            $product->bullet_point_4                 = $value[82];
            $product->maintenance_videos             = $value[83];
            $product->maintenance_video_title_1      = $value[84];
            $product->maintenance_video_url_1        = $value[85];
            $product->maintenance_video_title_2      = $value[86];
            $product->maintenance_video_url_2        = $value[87];
            $product->maintenance_video_title_3      = $value[88];
            $product->maintenance_video_url_3        = $value[89];
            $product->maintenance_video_title_4      = $value[90];
            $product->maintenance_video_url_4        = $value[91];
            $product->stock_status                   = $value[92];
            $product->related_products               = $value[93];
            $product->configurable_product_parent_id = $value[94];
            $product->save();

            if ($value[78]) {
                $relatedCategoryIds = explode(',', $value[78]);
                collect($relatedCategoryIds)->each(function ($item) use ($product) {
                    if (!is_numeric($item)) {
                        Log::debug('non INTEGER product category id', [
                            'data' => [
                                'product_id'  => $product->id,
                                'category_id' => $item,
                            ],
                        ]);
                        return;
                    }
                    $category = Category::find($item);

                    if (!$category) {
                        Log::debug('non existing product category id', [
                            'data' => [
                                'product_id'  => $product->id,
                                'category_id' => $item,
                            ],
                        ]);
                        return;
                    }

                    $product->categories()->sync($item);
                });
            }
        }
    }
};
