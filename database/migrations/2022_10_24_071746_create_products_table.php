<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //for the first time my migration looks this way because I dont 
        //know much about previous data types required and nullable possible enum values 
        //so for the first time just try to parse it, we can research the database later and fix it  using mysql queries like group by and so on
        //but the DB has almost 100 columns and it require many time to do it so i wil not do it now
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('type_id');
            $table->string('sku');
            $table->string('opera_sku')->nullable();
            $table->string('old_sku')->nullable();
            $table->string('override_opera')->nullable();
            $table->string('name');
            $table->integer('inlet')->nullable();
            $table->integer('outlet')->nullable();
            $table->integer('hose_type')->nullable();
            $table->integer('angle_in_deg')->nullable();
            $table->integer('max_lpm')->nullable();
            $table->integer('voltage')->nullable();
            $table->integer('material')->nullable();
            $table->integer('bar')->nullable();
            $table->integer('o_ring_thickness')->nullable();
            $table->integer('diameter')->nullable();
            $table->integer('colour')->nullable();
            $table->integer('rpm')->nullable();
            $table->integer('status');
            $table->string('url_key');
            $table->integer('visibility');
            $table->boolean('clearance')->nullable();
            $table->integer('max_temperature')->nullable();
            $table->text('description')->nullable();
            $table->text('short_description')->nullable();
            $table->text('tech_spec_1')->nullable();
            $table->text('tech_spec_2')->nullable();
            $table->text('tech_spec_3')->nullable();
            $table->json('product_videos')->nullable();
            $table->integer('nozzle_value')->nullable();
            $table->integer('nozzle_size')->nullable();
            $table->string('foam_value')->nullable();
            $table->boolean('is_featured')->nullable();
            $table->string('featured_position')->nullable();
            $table->string('hose_clamp_size')->nullable();
            $table->string('office_size')->nullable();
            $table->string('shoe_size')->nullable();
            $table->string('thread')->nullable();
            $table->string('size_and_angle')->nullable();
            $table->string('inlet_outlet')->nullable();
            $table->string('clothing_size')->nullable();
            $table->string('wheel_style')->nullable();
            $table->string('flow_and_pressure')->nullable();
            $table->string('country_of_manufacture')->nullable();
            $table->integer('select_nozzle_size')->nullable();
            $table->integer('dn_internal_diameter')->nullable();
            $table->string('currency')->nullable();
            $table->string('pack_size')->nullable();
            $table->string('easyturn')->nullable();
            $table->boolean('priority')->default(false);
            $table->string('manufacturer_number_1')->nullable();
            $table->string('manufacturer_number_2')->nullable();
            $table->string('manufacturer_number_3')->nullable();
            $table->string('manufacturer_number_4')->nullable();
            $table->string('manufacturer_number_5')->nullable();
            $table->string('manufacturer_number_6')->nullable();
            $table->string('manufacturer_number_7')->nullable();
            $table->string('manufacturer_number_8')->nullable();
            $table->string('manufacturer_number_9')->nullable();
            $table->string('manufacturer_number_10')->nullable();
            $table->integer('hose_application')->nullable();
            $table->integer('hose_inlet')->nullable();
            $table->integer('hose_outlet')->nullable();
            $table->integer('hose_length')->nullable();
            $table->integer('hose_colour')->nullable();
            $table->string('price');
            $table->string('special_price');
            $table->boolean('poa')->default(false);
            $table->string('poa_price')->nullable()->default(null);
            $table->string('msrp')->nullable()->default(null);
            $table->string('meta_title')->nullable()->default(null);
            $table->text('meta_keywords')->nullable()->default(null);
            $table->string('meta_description')->nullable()->default(null);
            $table->string('pdf_title_1')->nullable()->default(null);
            $table->string('pdf_title_2')->nullable()->default(null);
            $table->string('pdf_title_3')->nullable()->default(null);
            $table->string('pdf_title_4')->nullable()->default(null);
            $table->string('categories')->nullable()->default(null);
            $table->string('bullet_point_1')->nullable()->default(null);
            $table->string('bullet_point_2')->nullable()->default(null);
            $table->string('bullet_point_3')->nullable()->default(null);
            $table->string('bullet_point_4')->nullable()->default(null);
            $table->json('maintenance_videos')->nullable()->default(null);
            $table->string('maintenance_video_title_1')->nullable()->default(null);
            $table->string('maintenance_video_url_1')->nullable()->default(null);
            $table->string('maintenance_video_title_2')->nullable()->default(null);
            $table->string('maintenance_video_url_2')->nullable()->default(null);
            $table->string('maintenance_video_title_3')->nullable()->default(null);
            $table->string('maintenance_video_url_3')->nullable()->default(null);
            $table->string('maintenance_video_title_4')->nullable()->default(null);
            $table->string('maintenance_video_url_4')->nullable()->default(null);
            $table->string('stock_status')->nullable()->default(null);
            $table->string('related_products')->nullable()->default(null);
            $table->string('configurable_product_parent_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
